def imagenApp = "ci-app"
def imagenDb = "ci-db"
def host = "localhost:5000"
def customImagenApp = ""
def customImagenDb = ""
def dockerFolder = "docker"
def cloudFolder = "cloud"

pipeline {

	agent any
	
	tools { 
        maven "mvn35" 
        jdk "java8"
    }

	stages {
	
		stage ("Inicializacion") {
            steps {
				echo "PATH = ${PATH}"
				echo "M2_HOME = ${M2_HOME}"
				echo "Inicializamos jenkins para usar docker"
				sh "eval \$(minikube docker-env)"
            }
        }
	    
	    stage ("Construccion") {
            steps {
            	echo "Inicio maven"
                sh "mvn package -Dentorno=dev -P construccion"
                echo "Fin maven" 
            }
        }
        
        stage("Push de las imagenes al Registro privado") {
        	steps {
				script {
			        docker.withRegistry("http://${host}") {
			        	echo "Construimos ${imagenApp}"
			        	customImagenApp = docker.build("${imagenApp}", "-f ${dockerFolder}/Dockerfile-app --no-cache .")
			        	echo "Subimos ${imagenApp} al registro privado"
			            customImagenApp.push()
			            
			            echo "Construimos ${imagenDb}"
			        	customImagenDb = docker.build("${imagenDb}", "-f ${dockerFolder}/Dockerfile-db --no-cache .")
			        	echo "Subimos ${imagenDb} al registro privado"
			            customImagenDb.push()
			        }
		        }
	        }
	    }
	    
	    stage("Deploy Minikube") {
	    	steps {
	    		script {
		    		try {
						echo "Creacion de almacenamiento"
		    			sh "kubectl apply -f ${cloudFolder}/PersistentVolume.yaml"
					} catch (exc) {
						echo "Almacenamiento ya existe"
					}
		    		
		    		try {
		    			echo "Peticion de almacenamiento"
		    			sh "kubectl apply -f ${cloudFolder}/PersistentVolumeClaim.yaml"
		    		} catch (exc) {
		    			echo "Almacenamiento ya en uso"
		    		}
		    		
		    		try {
		    			echo "Se aplica ingress para el dominio 'gincol.blog.com'"
		    			sh "kubectl apply -f ${cloudFolder}/Ingress.yaml"
		    		} catch (exc) {
		    			echo "Ingress error"
		    		}
		    		
		    		try {
		    			echo "Creamos statefulset y servicio ${imagenDb}"
			    		sh "kubectl create -f ${cloudFolder}/DB.yaml"
						sh "sleep 100"
						echo "Enlazamos elementos del cluster de ${imagenDb}"
						sh "kubectl exec ci-db-ss-0 -c ci-db-container -- mongo --eval 'rs.initiate({_id: \"CiRepSet\", version: 1, members: [ {_id: 0, host: \"ci-db-ss-0.ci-db.default.svc.cluster.local:27017\"}, {_id: 1, host: \"ci-db-ss-1.ci-db.default.svc.cluster.local:27017\"}, {_id: 2, host: \"ci-db-ss-2.ci-db.default.svc.cluster.local:27017\"} ]});'"
		    		} catch (exc) {
						echo "Ya existe, lo recreamos"
						sh "kubectl replace --force --cascade=true -f ${cloudFolder}/DB.yaml"
						sh "sleep 100"
						echo "Enlazamos elementos del cluster de ${imagenDb}"
						sh "kubectl exec ci-db-ss-0 -c ci-db-container -- mongo --eval 'rs.reconfig({_id: \"CiRepSet\", version: 1, members: [ {_id: 0, host: \"ci-db-ss-0.ci-db.default.svc.cluster.local:27017\"}, {_id: 1, host: \"ci-db-ss-1.ci-db.default.svc.cluster.local:27017\"}, {_id: 2, host: \"ci-db-ss-2.ci-db.default.svc.cluster.local:27017\"} ]}, {force : true});'"
		    		}
		    		
		    		try {
		    			echo "Creamos ConfigMap"
		    			sh "kubectl apply -f ${cloudFolder}/ConfigMap.yaml"
		    		} catch (exc) {
		    			echo "Error al lanzar ConfigMap"
		    		}
		    		
		    		try {
		    			echo "Creamos deployment y servicio ${imagenApp}"
		    			sh "kubectl apply -f ${cloudFolder}/APP.yaml"
		    		} catch (exc) {
		    			echo "Error al lanzar el deploy/service de APP"
		    		}
		    		
		    	}
		    }
	    }
	    
	    stage("Borrado de imagenes") {
	    	steps {
	    		script {
	    			try {
		    			echo "Borramos imagen ${imagenApp}"
		    			sh "docker rmi ${imagenApp}"
	    			} catch (exc) {
		    			echo "no se puede borrar imagen ${imagenApp}"
		    		}
	    			
		    		try {
		    			echo "Borramos imagen ${imagenDb}"
		    			sh "docker rmi ${imagenDb}"
	    			} catch (exc) {
		    			echo "no se puede borrar imagen ${imagenDb}"
		    		}
		    		
		    		try {
		    			echo "Borramos imagenes obsoletas"
		    			sh "docker rmi \$(docker images --quiet --filter 'dangling=true')"
		    		} catch (exc) {
		    			echo "No se pueden borrar imagenes obsoletas"
		    		}
	    		}
	    	}
	    }
	    
    }

	post {
        always {
			echo "Fin Proceso"
        }
        success {
            echo "Fin OK";
        }
        unstable {
            echo "Fin inestable";
        }
        failure {
            echo "Fin KO";
        }
    }
}