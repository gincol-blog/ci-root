package es.gincol.blog.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import es.gincol.blog.model.User;
import es.gincol.blog.repository.UserRepository;
import es.gincol.blog.service.SequenceService;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*", allowCredentials = "true")
@RestController
@RequestMapping("/users")
public class UserController {

	private static final Logger log = LoggerFactory.getLogger(UserController.class);

	@Value("${sequence.users.name}")
	private String sequenceUsersName;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
    SequenceService sequenceService;

	@ApiOperation(value="Devuelve todos los usuarios")
	@GetMapping(path = "/all")
	public @ResponseBody List<User> getAllUsers() {
		log.info("Busqueda de todos los usuarios");
		return userRepository.findAll();
	}

	@ApiOperation(value="Devuelve el usuario asociado al username recibido")
	@GetMapping(path = "/{userId}")
	public @ResponseBody User getUserByUserId(@PathVariable("userId") String userId) {
		log.info("Busqueda del usuario " + userId);
		return userRepository.findByUserId(userId);
	}
	
	@ApiOperation(value="Inserta un usuario (o lo modifica si username ya existe) a partir de los datos recibidos. NO requiere id")
	@PostMapping(path = "/add")
	public @ResponseBody String addUser(@RequestParam String username, @RequestParam String password) {
		log.info("Insertando o modificando usuario con username: " + username);
		String retorno = "";
		User user = userRepository.findByUsername(username);
		if(user!=null) {
			user.setPassword(password);
			retorno = "Usuario modificado";
		} else {
			user = new User(Integer.toString(sequenceService.getNextSequence(sequenceUsersName)), username, password);
			retorno = "Usuario insertado";
		}
		userRepository.save(user);
		log.info(retorno);
		return retorno;
	}
	
	@ApiOperation(value="Elimina el usuario asociado al id recibido")
	@PostMapping(path = "/delete/{id}")
	public @ResponseBody String deleteUser(@PathVariable("id") String id) {
		log.info("Elimina usuario con id: " + id);
		userRepository.deleteById(id);
		log.info("Usuario eliminado");
		return "Usuario eliminado";
	}
	
	@ApiOperation(value="Elimina el usuario asociado al id recibido")
	@PostMapping(path = "/deleteByUsername/{username}")
	public @ResponseBody String deleteUserByUsename(@PathVariable("username") String username) {
		log.info("Elimina usuario con username: " + username);
		User user = userRepository.findByUsername(username);
		userRepository.delete(user);
		log.info("Usuario eliminado");
		return "Usuario eliminado";
	}

}
