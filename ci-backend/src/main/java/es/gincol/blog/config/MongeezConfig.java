package es.gincol.blog.config;

import org.mongeez.Mongeez;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.mongodb.Mongo;

@Configuration
public class MongeezConfig {

	private static final Logger log = LoggerFactory.getLogger(MongeezConfig.class);

	@Value("${spring.data.mongodb.database}")
	private String mongoDb;
	
	@Value("${mongeez.initialLoadingFile}")
	private String initialLoadingFile;
	
	@Bean(initMethod = "process")
	public Mongeez loadInitialData(final Mongo mongo) {
		log.info("Initializing default data from: " + initialLoadingFile);
		if(initialLoadingFile!= null && !initialLoadingFile.isEmpty()) {
			final Mongeez mongeez = new Mongeez();
			mongeez.setMongo(mongo);
			mongeez.setDbName(mongoDb);
			mongeez.setFile(new ClassPathResource(initialLoadingFile));
			return mongeez;
		}
		return null;		
	}

}
