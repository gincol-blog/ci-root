package es.gincol.blog.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import es.gincol.blog.model.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
	public User findByUserId(String userId);
	public User findByUsername(String username);
    public List<User> findAll();
}
