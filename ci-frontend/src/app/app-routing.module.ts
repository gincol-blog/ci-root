import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { UserEditComponent } from "./users/user-edit/user-edit.component";
import { UserDetailComponent } from "./users/user-detail/user-detail.component";
import { UsersComponent } from "./users/users.component";
import { UsersStartComponent } from "./users/users-start/users-start.component";
import { InicioComponent } from "./inicio/inicio.component";
import { CanDeactivateGuard } from "./users/user-edit/can-deactivate-guard.service";

const appRoutes: Routes = [
    { path: '', redirectTo: '/inicio', pathMatch: 'full' },
    { path: 'inicio', component: InicioComponent },
    {
        path: 'users', component: UsersComponent, children: [
            { path: '', component: UsersStartComponent },
            { path: 'new', component: UserEditComponent },
            { path: ':id', component: UserDetailComponent },
            { path: ':id/edit', component: UserEditComponent, canDeactivate: [CanDeactivateGuard] }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { useHash: true, onSameUrlNavigation: 'reload' } )
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {

}