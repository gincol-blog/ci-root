import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { UsersService } from '../users.service';
import { User } from '../../model/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {
  users: User[];
  private subscription: Subscription;

  constructor(private usersService: UsersService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.subscription = this.usersService.getUsers()
      .subscribe(data => {
        this.users = data;
      });
  }
  
  ngOnDestroy() {
    if (this.subscription){
      this.subscription.unsubscribe();
    }
  }

  onNewUser() {
    this.router.navigate(['new'], { relativeTo: this.route });
  }

}
