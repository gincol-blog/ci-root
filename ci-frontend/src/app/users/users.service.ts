import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { environment } from '../../environments/environment';
import { User } from '../model/user';

const httpOptions = {
  //headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  headers: new HttpHeaders({ 'accept': '*/*' })
};


@Injectable({
  providedIn: 'root'
})
export class UsersService {
  
  private userAllUrl = environment.userAllUrl;
  private userUrl = environment.userUrl;
  private userAddUrl = environment.userAddUrl;
  private userDeleteUrl = environment.userDeleteUrl;
  user: User;
 
  constructor(private http: HttpClient) { }

  public getUsers() {
    return this.http.get<User[]>(this.userAllUrl, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  public getUser(userId: string) {
    return this.http.get<User>(this.userUrl + userId, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  public addUser(username: string, password: string){
    return this.http.post(this.userAddUrl + "?username=" + username + "&password=" + password, null, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  public deleteUser(userId: string) {
    return this.http.post(this.userDeleteUrl + userId, null, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('Error:', error.error.message);
    } else {
      console.error(
        `Código retornado por el Backend ${error.status}, ` +
        `cuerpo: ${error.error}`);
    }
    return throwError(
      'Algo no funciona; por favor pruebe más tarde.');
  };

}
