import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { User } from '../../model/user';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit, OnDestroy {
  user: User;
  index: string;
  private subscription: Subscription;

  constructor(private usersService: UsersService,
    private route: ActivatedRoute, private router: Router) { 
    }

  ngOnInit() {
    this.subscription = this.route.params.subscribe(
      (params: Params) => {
        this.index = params['id'];
        this.usersService.getUser(this.index)
          .subscribe(data => {
            this.user = data;
          });
      }
    );
  }

  onDeleteUser() {
    this.subscription = this.usersService.deleteUser(this.user.username).subscribe();
    this.router.navigate(['/']);
  }

  onEditUser() {
    this.router.navigate(['edit'], { relativeTo: this.route, queryParamsHandling: 'preserve' });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
