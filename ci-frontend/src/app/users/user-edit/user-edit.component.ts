import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UsersService } from '../users.service';
import { Subscription } from 'rxjs';
import { CanComponentDeactivate } from './can-deactivate-guard.service';
import { User } from '../../model/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit, OnDestroy, CanComponentDeactivate {
  
  private subscription: Subscription;
  user: User;
  usernameForm = '';
  passwordForm = '';
  changesSaved = false;

  constructor(private usersService: UsersService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.fragment.subscribe();

    if (this.route.snapshot.params['id']) {
      this.subscription = this.route.params.subscribe(
        (params: Params) => {
          this.usersService.getUser(params['id'])
            .subscribe(data => {
              this.user = data;
              this.usernameForm = this.user.username;
              this.passwordForm = this.user.password;
            });
        }
      );
    } else {
      this.usernameForm = '';
      this.passwordForm = '';
    }
  }

  onAddUser(){
    this.subscription = this.usersService.addUser(this.usernameForm, this.passwordForm).subscribe();
    this.changesSaved = true;
    this.router.navigate(['/']);
  }

  ngOnDestroy(){
    if (this.subscription){
      this.subscription.unsubscribe();
    }
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {

    if ((this.usernameForm !== this.user.username || this.passwordForm !== this.user.password) &&
      !this.changesSaved) {
      return confirm('¿Seguro que quiere descartar los cambios?');
    } else {
      return true;
    }
  }
}
