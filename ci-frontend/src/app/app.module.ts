import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AppRoutingModule } from './app-routing.module';
import { UsersStartComponent } from './users/users-start/users-start.component';
import { UsersComponent } from './users/users.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { UserEditComponent } from './users/user-edit/user-edit.component';
import { UserListComponent } from './users/user-list/user-list.component';
import { UsersService } from './users/users.service';
import { InicioComponent } from './inicio/inicio.component';
import { UserItemComponent } from './users/user-list/user-item/user-item.component';
import { CanDeactivateGuard } from './users/user-edit/can-deactivate-guard.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    UsersStartComponent,
    UsersComponent,
    UserDetailComponent,
    UserEditComponent,
    UserListComponent,
    InicioComponent,
    UserItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [UsersService, CanDeactivateGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
