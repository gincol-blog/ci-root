export class User {
    public databaseId: string;
    public userId: string;
    public username: string;
    public password: string;

    constructor(databaseId: string, userId: string, username: string, password: string){
        this.databaseId = databaseId;
        this.userId = userId;
        this.username = username;
        this.password = password;
    }
}
