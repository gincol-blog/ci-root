export const environment = {
  production: false,
  entorno: 'dev',
  userAllUrl: 'https://gincol.blog.com/ci/users/all',
  userUrl: 'https://gincol.blog.com/ci/users/',
  userAddUrl: 'https://gincol.blog.com/ci/users/add',
  userDeleteUrl: 'https://gincol.blog.com/ci/users/deleteByUsername/'
};
