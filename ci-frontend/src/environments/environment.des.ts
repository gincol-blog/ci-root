export const environment = {
  production: false,
  entorno: 'des',
  userAllUrl: 'http://192.168.99.100:9090/users/all',
  userUrl: 'http://192.168.99.100:9090/users/',
  userAddUrl: 'http://192.168.99.100:9090/users/add',
  userDeleteUrl: 'http://192.168.99.100:9090/users/deleteByUsername/'
};
