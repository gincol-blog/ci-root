export const environment = {
  production: false,
  entorno: 'loc',
  userAllUrl: 'http://localhost:8080/users/all',
  userUrl: 'http://localhost:8080/users/',
  userAddUrl: 'http://localhost:8080/users/add',
  userDeleteUrl: 'http://localhost:8080/users/deleteByUsername/'
};
